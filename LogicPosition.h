#ifndef LOGICPOSITION_H
#define LOGICPOSITION_H
#include <string>
#include <iostream>
#include <vector>
#include "KeyAction.h"
using namespace std;


class LogicPosition{
	int n_opt,n_i,id;
	KeyAction *actions;					//@remove
	static vector<LogicPosition *> globalMap;
	static int locationCounter;
	public:
		vector<KeyAction> acvec;
		string text;
		string options;
		int locationStatus;
		LogicPosition *e,*n,*w,*s;		//@remove
		LogicPosition();
		LogicPosition(string text);
		void init();

		void setLogicDirection(char dir, LogicPosition *p); //@remove

		string getText();
		void setText(string s);
		void setOptionsText(string s);
		LogicPosition* goFromHere(char dir);		//@check_use
		void addKeyAction(KeyAction a);
		vector<KeyAction> getKeyActionVector();		//@check_use
		int getId();
		static LogicPosition* getLpById(int id);

};
#endif