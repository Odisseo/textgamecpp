#include "KeyAction.h"

KeyAction::KeyAction(){}

KeyAction::KeyAction(string s,string d, Command *c){
	setString(s,d);
	setFunction(c);
}

string KeyAction::getString(){
	return activator;
}
string KeyAction::getDescriber(){
	return describer;
}
void KeyAction::setString(string s,string d){
	activator=s;
	describer=d;
}

void KeyAction::setFunction(Command *c){
	command=c;
}
string KeyAction::fireFunction(){
	return command->fire();
}
string KeyAction::checkAction(string s){
	if(s.compare(activator)==0){
		string ret = fireFunction();
		return ret;
	}
	return "";
}
bool KeyAction::isAvailable(map<string,int> *current_status){
	for(auto &cond: showCondition){
		if(cond.second!=current_status->at(cond.first))
			return false;
	}
	return true;
}

void KeyAction::addShowCondition(string key, int requiredValue){
	showCondition.insert(pair<string,int>(key,requiredValue));
}