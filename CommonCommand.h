#ifndef COMMONCOMMAND_H
#define COMMONCOMMAND_H
#include "LogicPosition.h"
#include "GameController.h"
#include <iostream>


class CommonCommand{
public:
	static void moveToLocation(LogicPosition *to );
	static int getGameStatus(string s );
	static void setGameStatus(string s, int n);
};


#endif


