#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H
#include "TerminalView.h"
#include "LogicPosition.h"
#include <vector>
#include <string>
#include <map>
#include <functional>
#include <fstream>
#include <dirent.h>
#include <iostream>

using namespace std;

class TerminalView;

class GameController{
	TerminalView* view;
	static GameController* gc;
	string response;

	std::map<string,int> status;

	GameController();
	public:
		int i;
		LogicPosition* current;
		static GameController* getGc();
		void setPosition(LogicPosition *p);
		void init(TerminalView *t, LogicPosition *first_pos);
		void checkInput(string input_str);
		void looper();
		void addStatus(string key, int initval);
		int getStatus(string key);
		void setStatus(string key, int n);
		void setCurrentById(int id);
		
		void save(string filename);
		void load(string filename);

		bool existsFile(string name);
		void writeData(string name);
		void loadData(string name);

		void trapInDialog(DialogNode *start);
		int getInputNumber();

		map<string,int>* getAllStatus();

};
#endif