
#include "LogicPosition.h"

int LogicPosition::locationCounter=0;
vector<LogicPosition* > LogicPosition::globalMap;

LogicPosition::LogicPosition(){
	init();
}

LogicPosition::LogicPosition(string text){
	init();
	setText(text);
}
void LogicPosition::init(){
	n_opt=0;
	n_i=0;
	id=locationCounter++;
	LogicPosition::globalMap.push_back(this);
}

void LogicPosition::setLogicDirection(char dir, LogicPosition *p){
	switch(dir){
		case 'e':
			this->e=p;
		break;
		case 'n':
			this->n=p;
		break;
		case 'w':
			this->w=p;
		break;
		case 's':
			this->s=p;
		break;
	}
}
int LogicPosition::getId(){
	return id;
}

string LogicPosition::getText(){
	return this->text;
}
void LogicPosition::setText(string s){
	text=s;
}
void LogicPosition::setOptionsText(string s){
	options=s;
}

void LogicPosition::addKeyAction(KeyAction a){
	if(n_i==n_opt){
		n_opt++;
		//actions = (KeyAction *) realloc(actions, n_opt*sizeof(KeyAction));
		//actions[n_i]=a;
		acvec.resize(n_opt);
		acvec[n_i]=a;
		n_i++;
	}
	else{
		acvec[n_i]=a;
		n_i++;
	}
}

LogicPosition* LogicPosition::goFromHere(char dir){
	switch(dir){
		case 'e':
			return this->e;
		break;
		case 'n':
			return this->n;
		break;
		case 'w':
			return this->w;
		break;
		case 's':
			return this->s;
		break;
	}
}

vector<KeyAction> LogicPosition::getKeyActionVector(){
	return this->acvec;
}

LogicPosition* LogicPosition::getLpById(int id){
	if(id<globalMap.size())
		return globalMap.at(id);
	return NULL;
}