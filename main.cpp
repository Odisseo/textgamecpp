#include <functional>
#include <string>
#include <iostream>
#include "GameController.h"
#include "LogicPosition.h"
#include "TerminalView.h"	
#include "KeyAction.h"
#include "CommonCommand.h"
using namespace std;

void init(){
	LogicPosition *l001 = new LogicPosition();
	LogicPosition *l002 = new LogicPosition();
	LogicPosition *l003 = new LogicPosition();
	LogicPosition *l004 = new LogicPosition();
	LogicPosition *l005 = new LogicPosition();
	LogicPosition *l006 = new LogicPosition();
	LogicPosition *l007 = new LogicPosition();

	LogicPosition *l01 = new LogicPosition();


}


void first(){
	LogicPosition *l = new LogicPosition();
	LogicPosition *l2 = new LogicPosition();
	LogicPosition *l3 = new LogicPosition();
	TerminalView *t = new TerminalView();
	GameController *g  = GameController::getGc();

	g->addStatus("leva",0);
	g->addStatus("cond",0);


/*	LO TENGO PER STORIA
	string a = "asd";	
	Command c([&]() -> string { return a;});
	KeyAction ka("asd","asd:asd",&c);
	cout<<ka.fireFunction()<<endl;
*/

	l->setText("sei in una stanza: ci sono una porta e una leva e boh devo solo fare dei test");
	l2->setText("nella seconda stanza ci sono due porte, una ad est ed una ad ovest fai...");
	l3->setText("nella terza stanza le porte hanno rotto il cazzo");
	{

		Command *c = new Command([&]() -> string{
			CommonCommand::moveToLocation(l2);
			return "vieni dalla stanza est";
			});
		KeyAction *a  = new KeyAction("stanza", "stanza: entra nella stanza",c);
		l->addKeyAction(*a);

		Command *c2 = new Command([&]() -> string{
			if(g->getStatus("leva")==1)
				return "la leva è già stata azionata";
			else{
				g->setStatus("leva",1);
				return "tiri la leva e senti meccanismi che si muovono";
			}			
			});
		KeyAction *a2  = new KeyAction("leva", "leva: tira la leva",c2);
		l->addKeyAction(*a2);

		Command *c3 = new Command([&]() -> string{
			DialogNode *n = new DialogNode("Bene, allora facciamoci una bella chiaccherata","");
			DialogNode *n1 = new DialogNode("Perchè mi devi insultare? io non ti ho detto nulla","sei proprio un caino!");
			DialogNode *n2 = new DialogNode("no, non mangio il kebab","vuoi un kebab?");
			DialogNode *n3 = new DialogNode("ecco bravo, chiedi scusa, comunque non ho nulla da dirti", "scusa, non volevo offenderti...");
			DialogNode *n4 = new DialogNode("col cavolo!", "tira la leva per me!");
			DialogNode *n5 = new DialogNode("ok, tiro la leva per te","tireresti la leva per me?");
			DialogNode *nb = new DialogNode("asd asd","questisono44caratterivquestisono44caratteriX");
			DialogNode *nc = new DialogNode("asd asd","questisono44caratterivquestisono46caratteriXxx");

			n->addChoice(n1);n->addChoice(n2);
			n->addChoice(nb);n->addChoice(nc);
			n1->addChoice(n3);n1->addChoice(n4);
			n2->addChoice(n5);

			n5->addShowCondition("leva",0);

			n5->addAction([&]() -> string{
				g->setStatus("leva",1);
				return "il coso con cui parli tira la leva";
			});

			g->trapInDialog(n);
			//cout<<"here"<<endl;
			return "fine dialogo";

		});
		KeyAction *a3  = new KeyAction("dialogo", "dialogo: parla con il coso che parla",c3);
		l2->addKeyAction(*a3);

	}
	{
		Command *c = new Command([&]() -> string{
			CommonCommand::moveToLocation(l2);
			return "vieni dalla stanza ovest";
		});
		KeyAction *a = new KeyAction("torna", "torna: torna indietro",c);
		l3->addKeyAction(*a);
	}
	{
		Command *c = new Command([&]() -> string{
			CommonCommand::moveToLocation(l);
			return "";
		});
		Command *c1 = new Command( [&]() -> string{
			
			if(g->getStatus("leva")==1)
				CommonCommand::moveToLocation(l3);
			else
				return "La porta non si muove.";
			return " ";		
		});

		KeyAction *a = new KeyAction("est", "est: entra nella porta a est", c);
		KeyAction *a1 = new KeyAction("ovest", "ovest: torna entra nella porta ovest",c1);
		
		l2->addKeyAction(*a);
		l2->addKeyAction(*a1);

		l3->addKeyAction(*( new KeyAction("aziona","aziona <- cond=true",new Command([&]() -> string{
			g->setStatus("cond",1);
			return "attivato";
		}))));		
		l3->addKeyAction(*( new KeyAction("disattiva","aziona <- cond=false",new Command([&]() -> string{
			g->setStatus("cond",0);
			return "disattivato";
		}))));	
		KeyAction *k = new KeyAction("salta","salta a l",new Command([&]() -> string{
			CommonCommand::moveToLocation(l);
			return "jumped";
		}));
		k->addShowCondition("cond",1);
		l3->addKeyAction(*k);

	}

	g->init(t,l);

}

void second(){
	string wellcome = "                                                                                              \n WELLCOME                                                                                     \n         TO THIS                                                                              \n                UNEXPECTED                                                                    \n                          ADVENTURE                                                           \n                                                                                              \n                                                                                              \n                                                                                              \n                                                                                              \n            CERTO CHE A  MI ODIO PROPRIO                                                      \n                                                                                              \n                                                                                              \n                                A SCEGLIERE                                                   \n                                                                                              \n                                                                                              \n                                                                                              \n                                           QUESTO FORMATO                                     \n               ASCII ART?                                                                     \n                                                                                              \n                                                                                              \n";

	TerminalView *t = new TerminalView();
	GameController *g  = GameController::getGc();	
	t->setWellcome(wellcome);
	t->printWellcome();
}


int main(){
	second();
	first();

	return 0;
}
