#ifndef KEYACTION_H
#define KEYACTION_H
#include <string>
#include <stdlib.h>
#include <iostream>
#include <functional>
#include <typeinfo>
#include <vector>
#include <map>

using namespace std;


class DialogNode{
	public:
		string text,activator;
		vector<DialogNode *> choices;
		function<string()> funct;
		map<string,int> showCondition;

		DialogNode(string t,string act = ""){
			text=t;
			activator = act;
		}
		void addChoice(DialogNode *s){
			choices.push_back(s);
		}

		template<typename Func>
		void addAction(Func f){
			funct=f;
		}

		string fire(){
			return funct();
		}

		void addShowCondition(string s, int i){
			showCondition.insert(pair<string,int>(s,i));
		}

		vector<DialogNode *> getAvailableChoiches(map<string,int>current_status){
			vector<DialogNode * > ret;
			for(auto &next : choices ){
				bool nextIsAvail=true;
				for(auto &cond : next->showCondition){
					nextIsAvail =current_status.at(cond.first) == cond.second;
					if(!nextIsAvail)
						break;
				}
				if(nextIsAvail)
					ret.push_back(next);
			}
			return ret;
		}

};

class Command{
	public:
	function<string()> func;
	template<typename Func>
	Command(Func f){
		func=f;	
	}
	string fire(){
		string asd= func();
		return asd;
	}
};
class KeyAction{
	Command *command;
	string (*function)();
	string activator;
	string describer;
	map<string,int> showCondition;

public:
	KeyAction();
	KeyAction(string s,string d, Command *c);	
	string getString();
	string getDescriber();
	void setString(string s,string d);
	void setFunction(Command *c);
	string fireFunction();
	string checkAction(string s);
	bool isAvailable(map<string,int> *current_status);
	void addShowCondition(string key, int requiredValue);



};
#endif