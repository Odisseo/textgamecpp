#include "CommonCommand.h"

void CommonCommand::moveToLocation(LogicPosition *to){
	GameController::getGc()->setPosition(to);

}

int CommonCommand::getGameStatus(string s){
	return GameController::getGc()->getStatus(s);
}

void CommonCommand::setGameStatus(string s, int n){
	GameController::getGc()->setStatus(s,n);
}
