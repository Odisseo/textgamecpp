#ifndef TERMINALVIEW_H
#define TERMINALVIEW_H
#include <string>
#include <iostream>
#include <vector>
#include "LogicPosition.h"
#include "KeyAction.h"
#include "GameController.h"

using namespace std;


class TerminalView {
	static const string hul,hh,hur,hll,hlr,hv,clear,jmp_up,spl,spr,lh;
	string wellcome;
	public:
	TerminalView();
	void pclear();
	void drawBorder();
	void drawText(string textUp);
	void update(string response,bool takeInput=true);
	void updateDialog(string text, vector<DialogNode * > vec);
	void drawOptions();
	void takeNextInput();
	void writeResponse(string s);
	void setWellcome(string s);
	void printWellcome();
	void printHelp();
};		




#endif