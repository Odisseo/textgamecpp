#include "TerminalView.h"

const string TerminalView::hul="╔",TerminalView::hh="═",TerminalView::hur="╗",TerminalView::hll="╚",TerminalView::hlr="╝",TerminalView::hv="║",TerminalView::clear="\033[2J\033[;H",TerminalView::jmp_up="\033[0;0f",TerminalView::spl="╟",TerminalView::spr="╢",TerminalView::lh="─";		
TerminalView::TerminalView(){}

void TerminalView::pclear(){
	cout<<clear;
}
void TerminalView::drawBorder(){
	cout<<hul;
	for(int i=0; i<95; i++){
		cout<<hh;
	}
	cout<<hur;

	for(int i=2; i<20; i++){
		cout<<"\033["<<i<<";0f"<<hv;
		cout<<"\033["<<i<<";97f"<<hv;
	}
	cout<<endl<<hll;
	for(int i=0; i<95; i++){
		cout<<hh;
	}
	cout<<hlr;
}


void TerminalView::drawText(string textUp){
	int line_offset=1;
	for(int i = 0; i<940 ; i++){
		if(i%95==0){
			line_offset++;
			cout<<"\033["<<line_offset<<";1f"<<hv;
		}
		if(i<textUp.length())
			cout<<textUp.at(i);
		else
			cout<<' ';
	}
	cout<<endl<<spl;
	for(int i=0; i<95; i++){
		cout<<lh;
	}		
	cout<<spr;
}
void TerminalView::update(string response,bool takeInput){
	GameController *gc = GameController::getGc();
	pclear();
	drawBorder();
	drawText(gc->current->getText());
	drawOptions();
	if(! response.empty())
		writeResponse(response);
	if(takeInput)
		takeNextInput();
}

void TerminalView::updateDialog(string text, vector<DialogNode * > vec){
	pclear();
	drawBorder();	
	drawText(text);
	int r=13;
	int nchoice=1;
	cout<<"\033["<<r<<";2f";
	for(int i=0; i<vec.size(); i++ ){
		r+=1;
		cout<<nchoice<<"<- "<<vec[i]->activator<<"\033["<<r<<";2f";
		nchoice++;
	}
	//input handled by gamecontroller
}

void TerminalView::drawOptions(){
	int charCount=0;
	int rowCount=13;
	GameController *gc = GameController::getGc();
	vector<KeyAction> vec = gc->current->getKeyActionVector();
	cout<<"\033[13;2f";
	int tab=2;
	for (auto &comando : vec ){
		if(comando.isAvailable(gc->getAllStatus())){
			string descriptor = comando.getDescriber();
			cout<<"\033["<<rowCount<<";"<<tab<<"f"<<descriptor;
			if(tab==2)
				tab=47;
			else{
				tab=2;
				rowCount++;
			}
		}
	}

}
void TerminalView::writeResponse(string s){
	cout<<"\033[18;2f<<"<<s;
}

void TerminalView::takeNextInput(){
	cout<<"\033[19;2f>>";
	string input;
	cin>>input;
	GameController *g = GameController::getGc();
	g->checkInput(input);
	g->looper();

}

	void TerminalView::setWellcome(string s){wellcome=s;}
	void TerminalView::printWellcome(){
		pclear();
		cout<<"\e[8;22;99t";
		cout<<wellcome;
		cin.get();
		cin.get();
	}
void TerminalView::printHelp(){

}