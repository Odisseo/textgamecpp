#include "GameController.h"

GameController::GameController(){
	response="";
	response.clear();
	i=0;


}

GameController* GameController::gc=0;

GameController* GameController::getGc(){
	if(gc==0){
		gc = new GameController;
	}
	return gc;
}

void GameController::setPosition(LogicPosition *p){
	this-> current = p;
}


void GameController::init(TerminalView *t, LogicPosition *first_pos){
	view=t;
	current=first_pos;
	view->pclear();
	view->update(response);
}

void GameController::checkInput(string input_str){
	string ret;
	//check non game action
	if(input_str.compare("esci")==0){
		view->pclear();
		exit(0);
	}
	else if(input_str.compare("salva")==0){
		response="Inserire nome file di salvataggio: >>";
		string fname;
		view->update(response,false);
		cin>>fname;	
		if(existsFile(fname)==1){
			response="Il file esiste già. sovrascrivere? (y/n) >>";
			string resp;
			view->update(response,false);
			cin>>resp;
			if(resp.compare("y")!=0){
				response="salvataggio annullato";
				view->update(response);
			}
		}		
		save(fname);
	}
	else if(input_str.compare("carica")==0){
		response="Inserire nome file da caricare: >>";
		string fname;
		view->update(response,false);
		cin>>fname;	
		if(existsFile(fname)==0){
			response="Il file non esiste! male male";
			view->update(response);
		}
		load(fname);
	}	
	else if(input_str.compare("dbgstatus")==0) {
		for(map<string,int>::iterator it = status.begin(); it!=status.end();++it){
			cout<<it->first<<" "<<it->second<<endl;
		}		
	}
	else{
		vector<KeyAction> vec = current->getKeyActionVector();
		for(int i=0; i<vec.size(); i++){
			ret = vec[i].checkAction(input_str);
			if(! ret.empty()){
				response = ret;
				return;
			}
		}
		response.clear();
	}
}

void GameController::looper(){	 
	view->update(response);
}

void GameController::addStatus(string key, int initval){
	if(status.find(key)==status.end())
		status.insert(pair<string,int>(key,initval));
	else
		status[key]=initval;
}

int GameController::getStatus(string key){
	return status.at(key);
}
void GameController::setStatus(string key, int n){
	status[key]=n;
}

void GameController::setCurrentById(int id){
	current=LogicPosition::getLpById(id);
}

void GameController::save(string filename){
	writeData(filename);
}

void GameController::load(string filename){
	loadData(filename);
}


bool GameController::existsFile(string name){
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir ("./")) != NULL) {
	  while ((ent = readdir(dir)) != NULL) {
	  	string qst(ent->d_name); 
	  	if( qst.compare(name)==0){
	  		return true;
	  	}
	  }
	  closedir (dir);
	}
	return false;
}

void GameController::writeData(string name){
	ofstream file(name);
	if(file.is_open()){
		file<<current->getId();
		for(map<string,int>::iterator it = status.begin(); it!=status.end();++it){
			file<<it->first<<" "<<it->second<<endl;
		}
		file.close();
	}
}

void GameController::loadData(string name){
	ifstream file(name);
	string out;
	if(file.is_open()){
		getline(file,out);
		setCurrentById(stoi(out));
		while (!file.eof()) {
			string key,val;
			getline(file,out);
			size_t found = out.find(' ');
			if(found!=string::npos){
				key= out.substr(0,found);
				val = out.substr(found+1,out.length());
				addStatus(key,stoi(val));
			}

		}		
		file.close();
	}
	response="caricato!";
}

void GameController::trapInDialog(DialogNode *start){
	bool end=false;
	DialogNode * currentdialog=start;
	while(!end){
		vector<DialogNode *> nextNodes = currentdialog->getAvailableChoiches(status);
		view->updateDialog(currentdialog->text,nextNodes);	
		if(currentdialog->funct!=0){
			response = currentdialog->fire();
		}

		if(nextNodes.size()==0){
			cout<<"Premi invio per uscire dal dialgo ";
			cin.get();
			cin.get();
			end=true;
		}
		else{
			string input;
			int selected = getInputNumber();
			if(nextNodes.size()==0)
				end=true;
			else{
				if(selected>=0 && selected<nextNodes.size())
					currentdialog=nextNodes.at(selected);
			}
		}
	}
}

int GameController::getInputNumber(){
	int ret;
	try{
		string input;
		cin>>input;
		ret = stoi(input) -1 ;
	} catch(const std::invalid_argument& ia){
		return -1;
	}
	return ret;
}

map<string,int>* GameController::getAllStatus(){
	return &status;
}